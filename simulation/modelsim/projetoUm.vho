-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/17/2019 19:47:29"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	projetoUm IS
    PORT (
	S0 : OUT std_logic;
	A0 : IN std_logic;
	B0 : IN std_logic;
	A1 : IN std_logic;
	OP : IN std_logic;
	B1 : IN std_logic;
	A2 : IN std_logic;
	B2 : IN std_logic;
	A3 : IN std_logic;
	B3 : IN std_logic;
	S1 : OUT std_logic;
	S2 : OUT std_logic;
	S3 : OUT std_logic;
	OVF : OUT std_logic;
	a : OUT std_logic;
	e : OUT std_logic;
	f : OUT std_logic;
	c : OUT std_logic;
	d : OUT std_logic;
	g : OUT std_logic;
	b : OUT std_logic;
	a8 : OUT std_logic;
	e9 : OUT std_logic;
	f10 : OUT std_logic;
	c11 : OUT std_logic;
	d12 : OUT std_logic;
	g13 : OUT std_logic;
	b14 : OUT std_logic;
	a15 : OUT std_logic;
	e16 : OUT std_logic;
	f17 : OUT std_logic;
	c18 : OUT std_logic;
	d19 : OUT std_logic;
	g20 : OUT std_logic;
	b21 : OUT std_logic;
	OPM : OUT std_logic
	);
END projetoUm;

-- Design Ports Information
-- S0	=>  Location: PIN_AA1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1	=>  Location: PIN_W2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2	=>  Location: PIN_Y3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S3	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OVF	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a	=>  Location: PIN_U20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- e	=>  Location: PIN_U15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- f	=>  Location: PIN_Y15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c	=>  Location: PIN_V20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- d	=>  Location: PIN_U16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- g	=>  Location: PIN_P9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- b	=>  Location: PIN_Y20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a8	=>  Location: PIN_U21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- e9	=>  Location: PIN_Y22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- f10	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c11	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- d12	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- g13	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- b14	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- a15	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- e16	=>  Location: PIN_V14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- f17	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- c18	=>  Location: PIN_AA10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- d19	=>  Location: PIN_Y14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- g20	=>  Location: PIN_AB21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- b21	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OPM	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A0	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B0	=>  Location: PIN_U13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B1	=>  Location: PIN_V13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OP	=>  Location: PIN_AB13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B2	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A3	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B3	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF projetoUm IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_S0 : std_logic;
SIGNAL ww_A0 : std_logic;
SIGNAL ww_B0 : std_logic;
SIGNAL ww_A1 : std_logic;
SIGNAL ww_OP : std_logic;
SIGNAL ww_B1 : std_logic;
SIGNAL ww_A2 : std_logic;
SIGNAL ww_B2 : std_logic;
SIGNAL ww_A3 : std_logic;
SIGNAL ww_B3 : std_logic;
SIGNAL ww_S1 : std_logic;
SIGNAL ww_S2 : std_logic;
SIGNAL ww_S3 : std_logic;
SIGNAL ww_OVF : std_logic;
SIGNAL ww_a : std_logic;
SIGNAL ww_e : std_logic;
SIGNAL ww_f : std_logic;
SIGNAL ww_c : std_logic;
SIGNAL ww_d : std_logic;
SIGNAL ww_g : std_logic;
SIGNAL ww_b : std_logic;
SIGNAL ww_a8 : std_logic;
SIGNAL ww_e9 : std_logic;
SIGNAL ww_f10 : std_logic;
SIGNAL ww_c11 : std_logic;
SIGNAL ww_d12 : std_logic;
SIGNAL ww_g13 : std_logic;
SIGNAL ww_b14 : std_logic;
SIGNAL ww_a15 : std_logic;
SIGNAL ww_e16 : std_logic;
SIGNAL ww_f17 : std_logic;
SIGNAL ww_c18 : std_logic;
SIGNAL ww_d19 : std_logic;
SIGNAL ww_g20 : std_logic;
SIGNAL ww_b21 : std_logic;
SIGNAL ww_OPM : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \B0~input_o\ : std_logic;
SIGNAL \A0~input_o\ : std_logic;
SIGNAL \inst6|inst|inst|inst~combout\ : std_logic;
SIGNAL \OP~input_o\ : std_logic;
SIGNAL \B1~input_o\ : std_logic;
SIGNAL \A1~input_o\ : std_logic;
SIGNAL \inst6|inst1|inst1|inst~combout\ : std_logic;
SIGNAL \B2~input_o\ : std_logic;
SIGNAL \A2~input_o\ : std_logic;
SIGNAL \inst6|inst2|inst1|inst~combout\ : std_logic;
SIGNAL \B3~input_o\ : std_logic;
SIGNAL \A3~input_o\ : std_logic;
SIGNAL \inst6|inst3|inst1|inst~combout\ : std_logic;
SIGNAL \inst6|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst|inst16~0_combout\ : std_logic;
SIGNAL \inst|qrdd~0_combout\ : std_logic;
SIGNAL \inst|inst2~0_combout\ : std_logic;
SIGNAL \inst|inst28~0_combout\ : std_logic;
SIGNAL \inst|junk~0_combout\ : std_logic;
SIGNAL \inst|inst8~combout\ : std_logic;
SIGNAL \inst|nain~0_combout\ : std_logic;
SIGNAL \inst8|inst16~0_combout\ : std_logic;
SIGNAL \inst8|qrdd~0_combout\ : std_logic;
SIGNAL \inst8|inst2~0_combout\ : std_logic;
SIGNAL \inst8|inst28~0_combout\ : std_logic;
SIGNAL \inst8|junk~0_combout\ : std_logic;
SIGNAL \inst8|inst8~combout\ : std_logic;
SIGNAL \inst8|nain~0_combout\ : std_logic;
SIGNAL \inst7|inst16~0_combout\ : std_logic;
SIGNAL \inst7|qrdd~0_combout\ : std_logic;
SIGNAL \inst7|inst2~0_combout\ : std_logic;
SIGNAL \inst7|inst28~0_combout\ : std_logic;
SIGNAL \inst7|junk~0_combout\ : std_logic;
SIGNAL \inst7|inst8~combout\ : std_logic;
SIGNAL \inst7|nain~0_combout\ : std_logic;
SIGNAL \ALT_INV_B3~input_o\ : std_logic;
SIGNAL \ALT_INV_A3~input_o\ : std_logic;
SIGNAL \ALT_INV_B2~input_o\ : std_logic;
SIGNAL \ALT_INV_A2~input_o\ : std_logic;
SIGNAL \ALT_INV_OP~input_o\ : std_logic;
SIGNAL \ALT_INV_B1~input_o\ : std_logic;
SIGNAL \ALT_INV_A1~input_o\ : std_logic;
SIGNAL \ALT_INV_B0~input_o\ : std_logic;
SIGNAL \ALT_INV_A0~input_o\ : std_logic;
SIGNAL \inst7|ALT_INV_nain~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_inst8~combout\ : std_logic;
SIGNAL \inst7|ALT_INV_junk~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_inst28~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_qrdd~0_combout\ : std_logic;
SIGNAL \inst7|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst8|ALT_INV_nain~0_combout\ : std_logic;
SIGNAL \inst8|ALT_INV_inst8~combout\ : std_logic;
SIGNAL \inst8|ALT_INV_junk~0_combout\ : std_logic;
SIGNAL \inst8|ALT_INV_inst28~0_combout\ : std_logic;
SIGNAL \inst8|ALT_INV_qrdd~0_combout\ : std_logic;
SIGNAL \inst8|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_nain~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_inst8~combout\ : std_logic;
SIGNAL \inst|ALT_INV_junk~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_inst28~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_qrdd~0_combout\ : std_logic;
SIGNAL \inst|ALT_INV_inst16~0_combout\ : std_logic;
SIGNAL \inst6|inst3|inst1|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst6|inst2|inst1|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst6|inst1|inst1|ALT_INV_inst~combout\ : std_logic;
SIGNAL \inst6|inst|inst|ALT_INV_inst~combout\ : std_logic;

BEGIN

S0 <= ww_S0;
ww_A0 <= A0;
ww_B0 <= B0;
ww_A1 <= A1;
ww_OP <= OP;
ww_B1 <= B1;
ww_A2 <= A2;
ww_B2 <= B2;
ww_A3 <= A3;
ww_B3 <= B3;
S1 <= ww_S1;
S2 <= ww_S2;
S3 <= ww_S3;
OVF <= ww_OVF;
a <= ww_a;
e <= ww_e;
f <= ww_f;
c <= ww_c;
d <= ww_d;
g <= ww_g;
b <= ww_b;
a8 <= ww_a8;
e9 <= ww_e9;
f10 <= ww_f10;
c11 <= ww_c11;
d12 <= ww_d12;
g13 <= ww_g13;
b14 <= ww_b14;
a15 <= ww_a15;
e16 <= ww_e16;
f17 <= ww_f17;
c18 <= ww_c18;
d19 <= ww_d19;
g20 <= ww_g20;
b21 <= ww_b21;
OPM <= ww_OPM;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_B3~input_o\ <= NOT \B3~input_o\;
\ALT_INV_A3~input_o\ <= NOT \A3~input_o\;
\ALT_INV_B2~input_o\ <= NOT \B2~input_o\;
\ALT_INV_A2~input_o\ <= NOT \A2~input_o\;
\ALT_INV_OP~input_o\ <= NOT \OP~input_o\;
\ALT_INV_B1~input_o\ <= NOT \B1~input_o\;
\ALT_INV_A1~input_o\ <= NOT \A1~input_o\;
\ALT_INV_B0~input_o\ <= NOT \B0~input_o\;
\ALT_INV_A0~input_o\ <= NOT \A0~input_o\;
\inst7|ALT_INV_nain~0_combout\ <= NOT \inst7|nain~0_combout\;
\inst7|ALT_INV_inst8~combout\ <= NOT \inst7|inst8~combout\;
\inst7|ALT_INV_junk~0_combout\ <= NOT \inst7|junk~0_combout\;
\inst7|ALT_INV_inst28~0_combout\ <= NOT \inst7|inst28~0_combout\;
\inst7|ALT_INV_qrdd~0_combout\ <= NOT \inst7|qrdd~0_combout\;
\inst7|ALT_INV_inst16~0_combout\ <= NOT \inst7|inst16~0_combout\;
\inst8|ALT_INV_nain~0_combout\ <= NOT \inst8|nain~0_combout\;
\inst8|ALT_INV_inst8~combout\ <= NOT \inst8|inst8~combout\;
\inst8|ALT_INV_junk~0_combout\ <= NOT \inst8|junk~0_combout\;
\inst8|ALT_INV_inst28~0_combout\ <= NOT \inst8|inst28~0_combout\;
\inst8|ALT_INV_qrdd~0_combout\ <= NOT \inst8|qrdd~0_combout\;
\inst8|ALT_INV_inst16~0_combout\ <= NOT \inst8|inst16~0_combout\;
\inst|ALT_INV_nain~0_combout\ <= NOT \inst|nain~0_combout\;
\inst|ALT_INV_inst8~combout\ <= NOT \inst|inst8~combout\;
\inst|ALT_INV_junk~0_combout\ <= NOT \inst|junk~0_combout\;
\inst|ALT_INV_inst28~0_combout\ <= NOT \inst|inst28~0_combout\;
\inst|ALT_INV_qrdd~0_combout\ <= NOT \inst|qrdd~0_combout\;
\inst|ALT_INV_inst16~0_combout\ <= NOT \inst|inst16~0_combout\;
\inst6|inst3|inst1|ALT_INV_inst~combout\ <= NOT \inst6|inst3|inst1|inst~combout\;
\inst6|inst2|inst1|ALT_INV_inst~combout\ <= NOT \inst6|inst2|inst1|inst~combout\;
\inst6|inst1|inst1|ALT_INV_inst~combout\ <= NOT \inst6|inst1|inst1|inst~combout\;
\inst6|inst|inst|ALT_INV_inst~combout\ <= NOT \inst6|inst|inst|inst~combout\;

-- Location: IOOBUF_X0_Y18_N96
\S0~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|inst|inst|inst~combout\,
	devoe => ww_devoe,
	o => ww_S0);

-- Location: IOOBUF_X0_Y18_N62
\S1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|inst1|inst1|inst~combout\,
	devoe => ww_devoe,
	o => ww_S1);

-- Location: IOOBUF_X0_Y18_N45
\S2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|inst2|inst1|inst~combout\,
	devoe => ww_devoe,
	o => ww_S2);

-- Location: IOOBUF_X0_Y19_N39
\S3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|inst3|inst1|inst~combout\,
	devoe => ww_devoe,
	o => ww_S3);

-- Location: IOOBUF_X0_Y20_N56
\OVF~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|inst3|inst2~0_combout\,
	devoe => ww_devoe,
	o => ww_OVF);

-- Location: IOOBUF_X52_Y0_N36
\a~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|ALT_INV_inst16~0_combout\,
	devoe => ww_devoe,
	o => ww_a);

-- Location: IOOBUF_X43_Y0_N2
\e~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|ALT_INV_qrdd~0_combout\,
	devoe => ww_devoe,
	o => ww_e);

-- Location: IOOBUF_X36_Y0_N2
\f~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|inst2~0_combout\,
	devoe => ww_devoe,
	o => ww_f);

-- Location: IOOBUF_X44_Y0_N19
\c~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|ALT_INV_inst28~0_combout\,
	devoe => ww_devoe,
	o => ww_c);

-- Location: IOOBUF_X52_Y0_N19
\d~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|ALT_INV_junk~0_combout\,
	devoe => ww_devoe,
	o => ww_d);

-- Location: IOOBUF_X29_Y0_N19
\g~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|ALT_INV_inst8~combout\,
	devoe => ww_devoe,
	o => ww_g);

-- Location: IOOBUF_X48_Y0_N59
\b~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|ALT_INV_nain~0_combout\,
	devoe => ww_devoe,
	o => ww_b);

-- Location: IOOBUF_X52_Y0_N53
\a8~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|ALT_INV_inst16~0_combout\,
	devoe => ww_devoe,
	o => ww_a8);

-- Location: IOOBUF_X48_Y0_N93
\e9~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|ALT_INV_qrdd~0_combout\,
	devoe => ww_devoe,
	o => ww_e9);

-- Location: IOOBUF_X50_Y0_N53
\f10~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|inst2~0_combout\,
	devoe => ww_devoe,
	o => ww_f10);

-- Location: IOOBUF_X48_Y0_N76
\c11~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|ALT_INV_inst28~0_combout\,
	devoe => ww_devoe,
	o => ww_c11);

-- Location: IOOBUF_X50_Y0_N36
\d12~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|ALT_INV_junk~0_combout\,
	devoe => ww_devoe,
	o => ww_d12);

-- Location: IOOBUF_X46_Y0_N36
\g13~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|ALT_INV_inst8~combout\,
	devoe => ww_devoe,
	o => ww_g13);

-- Location: IOOBUF_X51_Y0_N36
\b14~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|ALT_INV_nain~0_combout\,
	devoe => ww_devoe,
	o => ww_b14);

-- Location: IOOBUF_X48_Y0_N42
\a15~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_inst16~0_combout\,
	devoe => ww_devoe,
	o => ww_a15);

-- Location: IOOBUF_X38_Y0_N19
\e16~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_qrdd~0_combout\,
	devoe => ww_devoe,
	o => ww_e16);

-- Location: IOOBUF_X46_Y0_N53
\f17~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|inst2~0_combout\,
	devoe => ww_devoe,
	o => ww_f17);

-- Location: IOOBUF_X22_Y0_N53
\c18~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_inst28~0_combout\,
	devoe => ww_devoe,
	o => ww_c18);

-- Location: IOOBUF_X36_Y0_N19
\d19~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_junk~0_combout\,
	devoe => ww_devoe,
	o => ww_d19);

-- Location: IOOBUF_X40_Y0_N76
\g20~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_inst8~combout\,
	devoe => ww_devoe,
	o => ww_g20);

-- Location: IOOBUF_X38_Y0_N53
\b21~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|ALT_INV_nain~0_combout\,
	devoe => ww_devoe,
	o => ww_b21);

-- Location: IOOBUF_X0_Y20_N39
\OPM~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \OP~input_o\,
	devoe => ww_devoe,
	o => ww_OPM);

-- Location: IOIBUF_X33_Y0_N41
\B0~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B0,
	o => \B0~input_o\);

-- Location: IOIBUF_X36_Y0_N35
\A0~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A0,
	o => \A0~input_o\);

-- Location: LABCELL_X41_Y1_N0
\inst6|inst|inst|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst|inst|inst~combout\ = ( \A0~input_o\ & ( !\B0~input_o\ ) ) # ( !\A0~input_o\ & ( \B0~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001111001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_A0~input_o\,
	combout => \inst6|inst|inst|inst~combout\);

-- Location: IOIBUF_X33_Y0_N92
\OP~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_OP,
	o => \OP~input_o\);

-- Location: IOIBUF_X33_Y0_N58
\B1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B1,
	o => \B1~input_o\);

-- Location: IOIBUF_X36_Y0_N52
\A1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1,
	o => \A1~input_o\);

-- Location: LABCELL_X41_Y1_N3
\inst6|inst1|inst1|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst1|inst1|inst~combout\ = ( \A1~input_o\ & ( !\B1~input_o\ $ (((!\OP~input_o\ & \B0~input_o\))) ) ) # ( !\A1~input_o\ & ( !\B1~input_o\ $ (((!\B0~input_o\) # (\OP~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010110100101101001011010010110111010010110100101101001011010010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_OP~input_o\,
	datab => \ALT_INV_B0~input_o\,
	datac => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst6|inst1|inst1|inst~combout\);

-- Location: IOIBUF_X34_Y0_N1
\B2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B2,
	o => \B2~input_o\);

-- Location: IOIBUF_X34_Y0_N52
\A2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2,
	o => \A2~input_o\);

-- Location: LABCELL_X41_Y1_N36
\inst6|inst2|inst1|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst2|inst1|inst~combout\ = ( \A2~input_o\ & ( !\B2~input_o\ $ (((!\OP~input_o\ & ((\B0~input_o\) # (\B1~input_o\))))) ) ) # ( !\A2~input_o\ & ( !\B2~input_o\ $ ((((!\B1~input_o\ & !\B0~input_o\)) # (\OP~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110010110100101011001011010010110011010010110101001101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_OP~input_o\,
	datad => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_A2~input_o\,
	combout => \inst6|inst2|inst1|inst~combout\);

-- Location: IOIBUF_X34_Y0_N18
\B3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B3,
	o => \B3~input_o\);

-- Location: IOIBUF_X34_Y0_N35
\A3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A3,
	o => \A3~input_o\);

-- Location: LABCELL_X41_Y1_N12
\inst6|inst3|inst1|inst\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst3|inst1|inst~combout\ = ( \B0~input_o\ & ( \A3~input_o\ & ( !\B3~input_o\ $ (!\OP~input_o\) ) ) ) # ( !\B0~input_o\ & ( \A3~input_o\ & ( !\B3~input_o\ $ (((!\OP~input_o\ & ((\B1~input_o\) # (\B2~input_o\))))) ) ) ) # ( \B0~input_o\ & ( 
-- !\A3~input_o\ & ( !\B3~input_o\ $ (\OP~input_o\) ) ) ) # ( !\B0~input_o\ & ( !\A3~input_o\ & ( !\B3~input_o\ $ ((((!\B2~input_o\ & !\B1~input_o\)) # (\OP~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111100000001111111100000000111110000111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B3~input_o\,
	datad => \ALT_INV_OP~input_o\,
	datae => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_A3~input_o\,
	combout => \inst6|inst3|inst1|inst~combout\);

-- Location: LABCELL_X41_Y1_N39
\inst6|inst3|inst2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst3|inst2~0_combout\ = ( \B3~input_o\ & ( !\OP~input_o\ ) ) # ( !\B3~input_o\ & ( (!\OP~input_o\ & (((\B0~input_o\) # (\B1~input_o\)) # (\B2~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000011110000011100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_OP~input_o\,
	datad => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst6|inst3|inst2~0_combout\);

-- Location: LABCELL_X41_Y1_N48
\inst|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst16~0_combout\ = ( \A1~input_o\ & ( (!\A0~input_o\) # ((!\A3~input_o\) # (\A2~input_o\)) ) ) # ( !\A1~input_o\ & ( !\A2~input_o\ $ (((\A0~input_o\ & !\A3~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111100110000110011110011000011111100111111111111110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_A0~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_A2~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst|inst16~0_combout\);

-- Location: LABCELL_X41_Y1_N51
\inst|qrdd~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|qrdd~0_combout\ = ( \A1~input_o\ & ( (!\A0~input_o\) # (\A3~input_o\) ) ) # ( !\A1~input_o\ & ( (!\A2~input_o\ & ((!\A0~input_o\))) # (\A2~input_o\ & (\A3~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100010111000101110001011100010111011101110111011101110111011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A3~input_o\,
	datab => \ALT_INV_A0~input_o\,
	datac => \ALT_INV_A2~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst|qrdd~0_combout\);

-- Location: LABCELL_X41_Y1_N27
\inst|inst2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst2~0_combout\ = ( \A1~input_o\ & ( (!\A3~input_o\ & ((!\A2~input_o\) # (\A0~input_o\))) ) ) # ( !\A1~input_o\ & ( (!\A2~input_o\ & (\A0~input_o\ & !\A3~input_o\)) # (\A2~input_o\ & ((\A3~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001010101000010100101010110101111000000001010111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A2~input_o\,
	datac => \ALT_INV_A0~input_o\,
	datad => \ALT_INV_A3~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst|inst2~0_combout\);

-- Location: LABCELL_X41_Y1_N30
\inst|inst28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst28~0_combout\ = ( \A1~input_o\ & ( (!\A3~input_o\ & ((\A2~input_o\) # (\A0~input_o\))) # (\A3~input_o\ & ((!\A2~input_o\))) ) ) # ( !\A1~input_o\ & ( ((!\A3~input_o\) # (!\A2~input_o\)) # (\A0~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110011111111111111001100111111111100000011111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_A0~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_A2~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst|inst28~0_combout\);

-- Location: LABCELL_X41_Y1_N33
\inst|junk~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|junk~0_combout\ = ( \A1~input_o\ & ( (!\A0~input_o\ & ((!\A3~input_o\) # (\A2~input_o\))) # (\A0~input_o\ & ((!\A2~input_o\))) ) ) # ( !\A1~input_o\ & ( (!\A0~input_o\ $ (\A2~input_o\)) # (\A3~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101011111010111110101111101011110111100101111001011110010111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A3~input_o\,
	datab => \ALT_INV_A0~input_o\,
	datac => \ALT_INV_A2~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst|junk~0_combout\);

-- Location: LABCELL_X41_Y1_N6
\inst|inst8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|inst8~combout\ = ( \A1~input_o\ & ( ((!\A0~input_o\) # (!\A2~input_o\)) # (\A3~input_o\) ) ) # ( !\A1~input_o\ & ( (\A2~input_o\) # (\A3~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111111111101111111011111110111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A3~input_o\,
	datab => \ALT_INV_A0~input_o\,
	datac => \ALT_INV_A2~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst|inst8~combout\);

-- Location: LABCELL_X41_Y1_N9
\inst|nain~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|nain~0_combout\ = ( \A1~input_o\ & ( (!\A0~input_o\ & ((!\A2~input_o\))) # (\A0~input_o\ & (!\A3~input_o\)) ) ) # ( !\A1~input_o\ & ( (!\A2~input_o\) # (!\A3~input_o\ $ (\A0~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111100111111001111110011111100111100010111000101110001011100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A3~input_o\,
	datab => \ALT_INV_A0~input_o\,
	datac => \ALT_INV_A2~input_o\,
	dataf => \ALT_INV_A1~input_o\,
	combout => \inst|nain~0_combout\);

-- Location: LABCELL_X48_Y2_N33
\inst8|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|inst16~0_combout\ = ( \inst6|inst|inst|inst~combout\ & ( (!\inst6|inst1|inst1|inst~combout\ & (!\inst6|inst3|inst1|inst~combout\ $ (!\inst6|inst2|inst1|inst~combout\))) # (\inst6|inst1|inst1|inst~combout\ & ((!\inst6|inst3|inst1|inst~combout\) # 
-- (\inst6|inst2|inst1|inst~combout\))) ) ) # ( !\inst6|inst|inst|inst~combout\ & ( (!\inst6|inst2|inst1|inst~combout\) # (\inst6|inst1|inst1|inst~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110101111101011111010101101101011011010110110101101101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst1|inst1|ALT_INV_inst~combout\,
	datab => \inst6|inst3|inst1|ALT_INV_inst~combout\,
	datac => \inst6|inst2|inst1|ALT_INV_inst~combout\,
	dataf => \inst6|inst|inst|ALT_INV_inst~combout\,
	combout => \inst8|inst16~0_combout\);

-- Location: LABCELL_X48_Y2_N36
\inst8|qrdd~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|qrdd~0_combout\ = ( \inst6|inst2|inst1|inst~combout\ & ( ((\inst6|inst1|inst1|inst~combout\ & !\inst6|inst|inst|inst~combout\)) # (\inst6|inst3|inst1|inst~combout\) ) ) # ( !\inst6|inst2|inst1|inst~combout\ & ( (!\inst6|inst|inst|inst~combout\) # 
-- ((\inst6|inst1|inst1|inst~combout\ & \inst6|inst3|inst1|inst~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000111110001111100011111000101110011011100110111001101110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst1|inst1|ALT_INV_inst~combout\,
	datab => \inst6|inst3|inst1|ALT_INV_inst~combout\,
	datac => \inst6|inst|inst|ALT_INV_inst~combout\,
	dataf => \inst6|inst2|inst1|ALT_INV_inst~combout\,
	combout => \inst8|qrdd~0_combout\);

-- Location: LABCELL_X48_Y2_N39
\inst8|inst2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|inst2~0_combout\ = ( \inst6|inst|inst|inst~combout\ & ( !\inst6|inst3|inst1|inst~combout\ $ (((!\inst6|inst1|inst1|inst~combout\ & \inst6|inst2|inst1|inst~combout\))) ) ) # ( !\inst6|inst|inst|inst~combout\ & ( (!\inst6|inst1|inst1|inst~combout\ & 
-- (\inst6|inst3|inst1|inst~combout\ & \inst6|inst2|inst1|inst~combout\)) # (\inst6|inst1|inst1|inst~combout\ & (!\inst6|inst3|inst1|inst~combout\ & !\inst6|inst2|inst1|inst~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100001001000010010000100100001011000110110001101100011011000110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst1|inst1|ALT_INV_inst~combout\,
	datab => \inst6|inst3|inst1|ALT_INV_inst~combout\,
	datac => \inst6|inst2|inst1|ALT_INV_inst~combout\,
	dataf => \inst6|inst|inst|ALT_INV_inst~combout\,
	combout => \inst8|inst2~0_combout\);

-- Location: LABCELL_X48_Y2_N30
\inst8|inst28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|inst28~0_combout\ = ( \inst6|inst2|inst1|inst~combout\ & ( (!\inst6|inst3|inst1|inst~combout\) # ((!\inst6|inst1|inst1|inst~combout\ & \inst6|inst|inst|inst~combout\)) ) ) # ( !\inst6|inst2|inst1|inst~combout\ & ( 
-- (!\inst6|inst1|inst1|inst~combout\) # ((\inst6|inst|inst|inst~combout\) # (\inst6|inst3|inst1|inst~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011111110111111101111111011111111001110110011101100111011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst1|inst1|ALT_INV_inst~combout\,
	datab => \inst6|inst3|inst1|ALT_INV_inst~combout\,
	datac => \inst6|inst|inst|ALT_INV_inst~combout\,
	dataf => \inst6|inst2|inst1|ALT_INV_inst~combout\,
	combout => \inst8|inst28~0_combout\);

-- Location: LABCELL_X48_Y2_N42
\inst8|junk~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|junk~0_combout\ = ( \inst6|inst|inst|inst~combout\ & ( (!\inst6|inst1|inst1|inst~combout\ & ((\inst6|inst2|inst1|inst~combout\) # (\inst6|inst3|inst1|inst~combout\))) # (\inst6|inst1|inst1|inst~combout\ & ((!\inst6|inst2|inst1|inst~combout\))) ) ) 
-- # ( !\inst6|inst|inst|inst~combout\ & ( (!\inst6|inst3|inst1|inst~combout\ & ((!\inst6|inst2|inst1|inst~combout\) # (\inst6|inst1|inst1|inst~combout\))) # (\inst6|inst3|inst1|inst~combout\ & ((!\inst6|inst1|inst1|inst~combout\) # 
-- (\inst6|inst2|inst1|inst~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110000111111111111000011111100111111111100000011111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst6|inst3|inst1|ALT_INV_inst~combout\,
	datac => \inst6|inst1|inst1|ALT_INV_inst~combout\,
	datad => \inst6|inst2|inst1|ALT_INV_inst~combout\,
	dataf => \inst6|inst|inst|ALT_INV_inst~combout\,
	combout => \inst8|junk~0_combout\);

-- Location: LABCELL_X48_Y2_N45
\inst8|inst8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|inst8~combout\ = ( \inst6|inst|inst|inst~combout\ & ( (!\inst6|inst1|inst1|inst~combout\ $ (!\inst6|inst2|inst1|inst~combout\)) # (\inst6|inst3|inst1|inst~combout\) ) ) # ( !\inst6|inst|inst|inst~combout\ & ( ((\inst6|inst2|inst1|inst~combout\) # 
-- (\inst6|inst3|inst1|inst~combout\)) # (\inst6|inst1|inst1|inst~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111101111111011111110111111101111011011110110111101101111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst1|inst1|ALT_INV_inst~combout\,
	datab => \inst6|inst3|inst1|ALT_INV_inst~combout\,
	datac => \inst6|inst2|inst1|ALT_INV_inst~combout\,
	dataf => \inst6|inst|inst|ALT_INV_inst~combout\,
	combout => \inst8|inst8~combout\);

-- Location: LABCELL_X48_Y2_N48
\inst8|nain~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst8|nain~0_combout\ = ( \inst6|inst2|inst1|inst~combout\ & ( (!\inst6|inst1|inst1|inst~combout\ & (!\inst6|inst3|inst1|inst~combout\ $ (\inst6|inst|inst|inst~combout\))) # (\inst6|inst1|inst1|inst~combout\ & (!\inst6|inst3|inst1|inst~combout\ & 
-- \inst6|inst|inst|inst~combout\)) ) ) # ( !\inst6|inst2|inst1|inst~combout\ & ( (!\inst6|inst1|inst1|inst~combout\) # ((!\inst6|inst3|inst1|inst~combout\) # (!\inst6|inst|inst|inst~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111011111110111111101111111010000110100001101000011010000110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|inst1|inst1|ALT_INV_inst~combout\,
	datab => \inst6|inst3|inst1|ALT_INV_inst~combout\,
	datac => \inst6|inst|inst|ALT_INV_inst~combout\,
	dataf => \inst6|inst2|inst1|ALT_INV_inst~combout\,
	combout => \inst8|nain~0_combout\);

-- Location: LABCELL_X41_Y1_N42
\inst7|inst16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst16~0_combout\ = ( \B3~input_o\ & ( (!\B2~input_o\ & ((!\B1~input_o\) # (!\B0~input_o\))) # (\B2~input_o\ & (\B1~input_o\)) ) ) # ( !\B3~input_o\ & ( (!\B2~input_o\ $ (\B0~input_o\)) # (\B1~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011101101110111101110110111011110111011100110011011101110011001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datad => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst7|inst16~0_combout\);

-- Location: LABCELL_X41_Y1_N45
\inst7|qrdd~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|qrdd~0_combout\ = ( \B3~input_o\ & ( ((!\B0~input_o\) # (\B1~input_o\)) # (\B2~input_o\) ) ) # ( !\B3~input_o\ & ( (!\B0~input_o\ & ((!\B2~input_o\) # (\B1~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011000010110000101100001011000011110111111101111111011111110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst7|qrdd~0_combout\);

-- Location: LABCELL_X41_Y1_N18
\inst7|inst2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst2~0_combout\ = ( \B3~input_o\ & ( (\B2~input_o\ & !\B1~input_o\) ) ) # ( !\B3~input_o\ & ( (!\B2~input_o\ & ((\B0~input_o\) # (\B1~input_o\))) # (\B2~input_o\ & (\B1~input_o\ & \B0~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001010111011001000101011101101000100010001000100010001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datad => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst7|inst2~0_combout\);

-- Location: LABCELL_X41_Y1_N21
\inst7|inst28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst28~0_combout\ = ( \B3~input_o\ & ( (!\B2~input_o\) # ((!\B1~input_o\ & \B0~input_o\)) ) ) # ( !\B3~input_o\ & ( ((!\B1~input_o\) # (\B0~input_o\)) # (\B2~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101111111011111110111111101111110101110101011101010111010101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst7|inst28~0_combout\);

-- Location: LABCELL_X41_Y1_N54
\inst7|junk~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|junk~0_combout\ = ( \B3~input_o\ & ( (!\B1~input_o\) # (!\B2~input_o\ $ (!\B0~input_o\)) ) ) # ( !\B3~input_o\ & ( (!\B2~input_o\ & ((!\B0~input_o\) # (\B1~input_o\))) # (\B2~input_o\ & (!\B1~input_o\ $ (!\B0~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011101101100110101110110110011011011101111011101101110111101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datad => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst7|junk~0_combout\);

-- Location: LABCELL_X41_Y1_N57
\inst7|inst8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|inst8~combout\ = ( \B3~input_o\ ) # ( !\B3~input_o\ & ( (!\B2~input_o\ & (\B1~input_o\)) # (\B2~input_o\ & ((!\B1~input_o\) # (!\B0~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011001110110011101100111011011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst7|inst8~combout\);

-- Location: LABCELL_X41_Y1_N24
\inst7|nain~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|nain~0_combout\ = ( \B3~input_o\ & ( (!\B0~input_o\ & ((!\B2~input_o\))) # (\B0~input_o\ & (!\B1~input_o\)) ) ) # ( !\B3~input_o\ & ( (!\B2~input_o\) # (!\B1~input_o\ $ (\B0~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011110011111111001111001111110000110011001111000011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B2~input_o\,
	datad => \ALT_INV_B0~input_o\,
	dataf => \ALT_INV_B3~input_o\,
	combout => \inst7|nain~0_combout\);

-- Location: MLABCELL_X49_Y13_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


